import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by Valeriy on 23.09.2016.
 */
public class Secondhomework {
    public static void main(String[] args) throws Exception{
        //System.out.println("Min: "+ Min(6,4,3));
        //System.out.println("FactWhile: "+FactWhile(6));
        //System.out.println("FactDoWhile: "+FactDoWhile(5));
        //System.out.println("FactFor: "+FactFor(7));
        //Array();
        //ArrayWithout5();
        //Calc();
        //MinAndMax();
        //ArraySumDiagonal();
        //Month();

    }
    static int Min(int a, int b, int c){
        System.out.println("Numbers: "+a+", "+b+", "+c);
        if(a<b & a<c) return a;
        else if(b<a & b<c) return b;
        else return c;
    }
    static int FactWhile (int a) {
        System.out.println("Factorial of "+a);
        int result=a;
        while (a > 1) {
            a--;
            result*=a;
        }
        return result;
    }
    static int FactDoWhile (int a) {
        System.out.println("Factorial of "+a);
        int result=a;
        do{
            a--;
            result*=a;
        }
        while (a > 1) ;
        return result;
    }
    static int FactFor (int a) {
        System.out.println("Factorial of "+a);
        for(int i=a;i>1;i--) {
            a *= i-1;
        }
        return a;
    }
    static void Array(){
        int arr[]= new int[20];
        for(int i=0;i<arr.length;i++){
            arr[i]=i+1;
        }
        System.out.println("Created array:");
        for (int i=0;i<10;i++) {
            System.out.print(arr[i] + " ");
        }
    }
    static void ArrayWithout5(){
        int arr [] = new int[50];
        System.out.println("Created array:");
        for (int i=0;i<arr.length;i++){
            arr[i]=(i+1)%10;
            System.out.print(arr[i]+" ");
        }
        System.out.println("");
        System.out.println("Array without 5:");
        for (int anArr : arr) {
            if (anArr != 5)
                System.out.print(anArr + " ");
        }
    }
    static void Calc() throws Exception{
        int a = 5,b = 8,result;
        System.out.println("A="+a+" B="+b);
        System.out.println("Enter operation:");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = reader.readLine();
        switch (s){
            case "/": System.out.println("A/B="+a/b);
                break;
            case "*": System.out.println("A*B="+a*b);
                break;
            case "+": System.out.println("A+B="+(a+b));
                break;
            case "-": System.out.println("A-B="+(a-b));
                break;
            default:
                System.out.println("Operation error");
        }
    }
    static void MinAndMax() {
        int arrOne[] = new int[20];
        int arrTwo[][] = new int[10][10];

        //crt first array
        System.out.println("First array:");
        for (int i = 0; i < arrOne.length; i++) {
            arrOne[i] = i;
            System.out.print(arrOne[i] + " ");

        }

        //crt second array
        System.out.println("");
        System.out.println("");
        System.out.println("Second array:");
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                arrTwo[i][j] = i + j;
                System.out.print(arrTwo[i][j]+" ");
            }
            System.out.println(" ");
        }

        //min+max first
        int min = arrOne[0];
        int max = arrOne[0];
        for (int anArrOne : arrOne) {
            if (anArrOne < min)
                min = anArrOne;
            else if (anArrOne > max)
                max = anArrOne;
        }
        System.out.println("");
        System.out.println("Min first array: " + min + "; Max first array: " + max + ";");

        //min+max second
        int mintwo = arrTwo[0][0];
        int maxtwo = arrTwo[0][0];
        for (int i = 0; i < 10 ; i++) {
            for (int j = 0; j < 10; j++) {
                if (arrTwo[i][j] < min) min = arrTwo[i][j];
                else  if (arrTwo[i][j]>max) maxtwo = arrTwo[i][j];
            }

        }
        System.out.println("");
        System.out.println("Min second array: " + mintwo + "; Max second array: " + maxtwo + ";");
    }
    static void ArraySumDiagonal(){
        System.out.println("Array:");

        //crt arr
        int arr[][]= new int[10][10];
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                arr[i][j] = 2*i + j;
                System.out.print(arr[i][j]+" ");
            }
            System.out.println(" ");
        }

        //calculate
        int summ=0;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if(i>j){
                 summ+=arr[i][j];
                }
            }
        }
        System.out.println("");
        System.out.println("Summ under main diagonal: "+summ);
    }
    static void Month(){

        //crt arr
        int arr[] = new int[31];
        for (int i=0;i<31;i++) {
            arr[i] = i+1;
        }

        for (int i=0;i<31;i++){
            if(((arr[i]%7)-5)==0) System.out.println(arr[i]+" day is Friday");
        }

        System.out.print("31 day is ");
        switch (arr[30]%7){
            case (0):
                System.out.println("Sunday");
                break;
            case (1):
                System.out.println("Monday");
                break;
            case (2):
                System.out.println("Tuesday");
                break;
            case (3):
                System.out.println("Wednesday");
                break;
            case (4):
                System.out.println("Thursday");
                break;
            case (5):
                System.out.println("Friday");
                break;
            default:
                System.out.println("Saturday");
        }
    }
}
